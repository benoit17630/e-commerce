<?php

namespace App\Twig\Extension;

use App\Entity\Company\Company;
use App\Repository\Company\CompanyRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CompanyExtension extends AbstractExtension
{
    public function __construct(private readonly CompanyRepository $companyRepository)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('company', [$this, 'company']),
        ];
    }

    public function company(): ?Company
    {
        return $this->companyRepository->findOneBy([], ['id' => 'DESC']);
    }
}
