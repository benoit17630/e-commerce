<?php

namespace App\Twig\Components;

use App\Repository\LanguageRepository;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
final class TopHeader
{
    public function __construct(private readonly LanguageRepository $languageRepository)
    {
    }

    public function getLocal()
    {
        return $this->languageRepository->findAll();
    }
}
