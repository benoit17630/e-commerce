<?php

namespace App\DataFixtures;

use App\Entity\Company\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // initialize faker
        $faker = Factory::create('fr_FR');

        // create a company
        $company = new Company();
        $company->setName($faker->company);
        $company->setLogo('logo.png');
        $company->setEmail($faker->email);
        $company->setPhone($faker->phoneNumber);
        $company->setSocialForm($faker->companySuffix);
        $company->setCapital($faker->randomNumber(5));
        $company->setRcsNumber('6454scmwjwjù');
        $company->setTvaNumber('065467879');
        // translate company fr
        $company->translate('fr')
            ->setHeadOffice($faker->address)
            ->setOpenDay($faker->dayOfWeek)
            ->setOpenTime($faker->time('H:i'))
        ;
        // translate company en
        $company->translate('en')
            ->setHeadOffice($faker->address)
            ->setOpenDay($faker->dayOfWeek)
            ->setOpenTime($faker->time('H:i'))
        ;
        $manager->persist($company);
        $company->mergeNewTranslations();

        $manager->flush();
    }
}
