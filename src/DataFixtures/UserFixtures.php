<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public function __construct(private readonly UserPasswordHasherInterface $hasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        // create 20 users! Bam! with faker
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 20; ++$i) {
            $user = new User();
            $user
                ->setEmail('user-'.$i.'@hotmail.com')
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setPhone($faker->phoneNumber)
                ->setPassword($this->hasher->hashPassword($user, 'password'))
                ->setRoles(['ROLE_USER']);
            $manager->persist($user);
            $this->addReference('user-'.$i, $user);
        }

        // create 1 admin
        $admin = new User();
        $admin
            ->setEmail('admin@hotmail.com')
            ->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setPhone($faker->phoneNumber)
            ->setPassword($this->hasher->hashPassword($admin, 'password'))
            ->setRoles(['ROLE_ADMIN'])
        ;
        $manager->persist($admin);
        $this->addReference('admin-1', $admin);

        // create 1 super admin
        $superAdmin = new User();
        $superAdmin
            ->setEmail('superadmin@hotmail.com')
            ->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setPhone($faker->phoneNumber)
            ->setPassword($this->hasher->hashPassword($superAdmin, 'password'))
            ->setRoles(['ROLE_SUPER_ADMIN'])
        ;
        $manager->persist($superAdmin);
        $this->addReference('superadmin-1', $superAdmin);
        $manager->flush();
    }
}
