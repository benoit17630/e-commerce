<?php

namespace App\Entity\Company;

use App\Repository\Company\CompanyTranslationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslationTrait;

#[ORM\Entity(repositoryClass: CompanyTranslationRepository::class)]
class CompanyTranslation implements TranslationInterface
{
    use TranslationTrait;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $openDay = null;

    #[ORM\Column(length: 255)]
    private ?string $openTime = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $headOffice = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOpenDay(): ?string
    {
        return $this->openDay;
    }

    public function setOpenDay(string $openDay): static
    {
        $this->openDay = $openDay;

        return $this;
    }

    public function getOpenTime(): ?string
    {
        return $this->openTime;
    }

    public function setOpenTime(string $openTime): static
    {
        $this->openTime = $openTime;

        return $this;
    }

    public function getHeadOffice(): ?string
    {
        return $this->headOffice;
    }

    public function setHeadOffice(string $headOffice): static
    {
        $this->headOffice = $headOffice;

        return $this;
    }
}
