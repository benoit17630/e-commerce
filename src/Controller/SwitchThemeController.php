<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SwitchThemeController extends AbstractController
{
    #[Route('/switch/theme/light', name: 'app_switch_theme_light')]
    public function index(Request $request): RedirectResponse
    {
        // create cookie with name theme and value light
        $response = new Response();
        $response->headers->setCookie(
            new Cookie(
                'theme',
                'light',
                time() + (365 * 24 * 60 * 60)
            )
        );
        $response->send();

        // redirect to the last page
        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    #[Route('/switch/theme/dark', name: 'app_switch_theme_dark')]
    public function dark(Request $request): RedirectResponse
    {
        // create cookie with name theme and value dark
        $response = new Response();
        $response->headers->setCookie(
            new Cookie(
                'theme',
                'dark',
                time() + (365 * 24 * 60 * 60)
            )
        );
        $response->send();

        // redirect to the last page
        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }
}
