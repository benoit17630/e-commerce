<?php

namespace App\Controller\Admin;

use App\Entity\Language;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LanguageField;

class LanguageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Language::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $acceptedLanguages = explode('|', $_ENV['APP_LOCALES']);
        return [
            IdField::new('id')->onlyOnIndex(),
            LanguageField::new('language')
                ->setRequired(true)
            ->includeOnly($acceptedLanguages),
        ];
    }
}
