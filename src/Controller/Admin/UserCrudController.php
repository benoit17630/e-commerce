<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->onlyOnIndex(),
            EmailField::new('email')
                ->setDisabled()
                ->setPermission('ROLE_ADMIN'),
            EmailField::new('email')
                ->setPermission('ROLE_SUPER_ADMIN')
                ->hideOnDetail(),
            TextField::new('fullName')
                ->setDisabled()->hideOnForm(),
            TextField::new('firstName')
                ->setDisabled()
                ->setPermission('ROLE_ADMIN')
                ->onlyOnForms(),
            TextField::new('firstName')
                ->setPermission('ROLE_SUPER_ADMIN')
                ->onlyOnForms(),
            TextField::new('lastName')
                ->setDisabled()
                ->setPermission('ROLE_ADMIN')
                ->onlyOnForms(),
            TextField::new('lastName')
                ->setPermission('ROLE_SUPER_ADMIN')
                ->onlyOnForms(),
            TelephoneField::new('phone')
                ->setPermission('ROLE_ADMIN')
                ->onlyOnDetail(),
            TelephoneField::new('phone')
                ->setPermission('ROLE_SUPER_ADMIN')
                ->onlyOnForms(),
            TextField::new('password')
                ->setPermission('ROLE_SUPER_ADMIN')
                ->onlyOnForms()
                ->setFormType(PasswordType::class),
            ChoiceField::new('roles')
                ->setChoices(User::ROLES)
                ->allowMultipleChoices(),
            BooleanField::new('isVerified')
                ->onlyOnIndex()
                ->renderAsSwitch(false),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $impersonate = Action::new('impersonate', false, 'fa fa-fw fa-user-lock')
            ->linkToUrl(function (User $entity) {
                return '/?_switch_user='.$entity->getEmail();
            })
        ;

        return parent::configureActions($actions)
            ->setPermission(Action::NEW, 'ROLE_SUPER_ADMIN')
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->setPermission(Action::DELETE, 'ROLE_SUPER_ADMIN')
            ->add(Crud::PAGE_INDEX, $impersonate)
            ->setPermission($impersonate, 'ROLE_SUPER_ADMIN')

        ;
    }
}
