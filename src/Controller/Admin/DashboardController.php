<?php

namespace App\Controller\Admin;

use App\Entity\Company\Company;
use App\Entity\Language;
use App\Entity\User;
use App\Repository\LanguageRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{_locale<%app.supported_locales%>}')]
class DashboardController extends AbstractDashboardController
{
    public function __construct(private readonly TranslatorInterface $translator,
        private readonly LanguageRepository $languageRepository)
    {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        $locales = $this->languageRepository->findAll();
        return Dashboard::new()
            ->setTitle('E Commerce')
            ->setFaviconPath('uploads/logo/logo.png')
            ->setTranslationDomain('admin')
            ->setLocales($locales)
        ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToUrl('Back to the website', 'fa fa-home', '/');
        // user menu
        yield MenuItem::linkToLogout('Logout', 'fa-solid fa-right-from-bracket');
        // create a section for admin
        yield MenuItem::section('Admin', 'fa fa-user');
        yield MenuItem::linkToCrud('Company', 'fa fa-building', Company::class);
        // sub menu for user
        yield MenuItem::subMenu('Users', 'fa fa-users')->setSubItems([
            MenuItem::linkToCrud('Users list', 'fa fa-list', User::class),
            MenuItem::linkToCrud('add User', 'fa fa-plus', User::class)
                ->setAction('new')
                ->setPermission('ROLE_SUPER_ADMIN'),
        ]);
        // submenu for language
        yield MenuItem::subMenu($this->translator->trans('Accepted language', domain: 'admin'), 'fa fa-language')
            ->setSubItems([
                MenuItem::linkToCrud($this->translator->trans('Language list', domain: 'admin'), 'fa fa-list', Language::class),
                MenuItem::linkToCrud($this->translator->trans('Add language', domain: 'admin'), 'fa fa-plus', Language::class)
                    ->setAction('new')
                    ->setPermission('ROLE_SUPER_ADMIN'),
            ]);
    }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            ->showEntityActionsInlined()
        ;
    }
}
