<?php

namespace App\Controller\Admin;

use App\Controller\Admin\Field\TranslationField;
use App\Entity\Company\Company;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Contracts\Translation\TranslatorInterface;

class CompanyCrudController extends AbstractCrudController
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public static function getEntityFqcn(): string
    {
        return Company::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('name'),
            ImageField::new('logo')
                ->setBasePath('uploads/logo')
                ->setUploadDir('public/uploads/logo')
                ->setUploadedFileNamePattern('[name]-[timestamp].[extension]')
                ->setRequired(false),
            TextField::new('email'),
            TelephoneField::new('phone'),
            TextField::new('socialForm'),
            MoneyField::new('capital')->setCurrency('EUR'),
            TextField::new('rcsNumber'),
            TextField::new('tvaNumber'),
            TranslationField::new('translations')->onlyOnForms(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
            ->remove('index', 'delete')
            ->remove('index', 'new')
        ;
    }
}
